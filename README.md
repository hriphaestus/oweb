# oweb


This repository is a university assignment repository. If you are looking at this, you are probably somebody from FEEIT. If you are not from FEEIT, you are still welcome to take a look at this repository.


## Folder Structure

To separate the different assignments, I came up with the following folder structure, which will be stored here, such as homework, projects, or any other tasks related to the subject.

- `homework` - all homework assignments will be stored here
- `projects` - more complex assignments, such as the project assignment we are going to be upgrading over time, will be stored, here
- `laboratory-exercises` - laboratory exercises and examples will be stored here
