const formRef = document.querySelector('form');
const typeSelect = document.querySelector('select');

typeSelect.addEventListener('change', (event) => {
  const labelRef = document.querySelector('label[for="message"]');
  if (event.target.value === 'message') {
    labelRef.textContent = 'Message';
  } else {
    labelRef.textContent = 'Testimonial';
  }
});

formRef.addEventListener('submit', (event) => {
  event.preventDefault();

  const formData = new FormData(formRef).entries();

  let formObj = {};

  for (const [key, val] of formData) {
    formObj[key] = val;
  }

  alert(
    'Here is the information you entered in the form:\n' +
      JSON.stringify(formObj, null, 2)
  );

  formRef.reset();
});
