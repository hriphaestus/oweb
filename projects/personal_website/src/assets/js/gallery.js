const photosData = [
  {
    id: 0,
    url: 'assets/img/gallery/drone_shot.jpg',
    description: 'A wonderful view. This photo was taken with a drone',
    likes: ['test', 'test2'],
    comments: [
      { user: 'test', body: 'this is amazing' },
      { user: 'user', body: 'wonderful' },
      { user: 'test2', body: 'wow 😲' },
    ],
  },
  {
    id: 1,
    url: 'assets/img/gallery/abstract_architecture.jpg',
    description: 'New architecture for today. Enjoy!',
    likes: ['test', 'test2', 'test700', 'wowsy'],
    comments: [
      { user: 'test', body: 'this is amazing' },
      { user: 'user', body: 'wonderful' },
    ],
  },
  {
    id: 2,
    url: 'assets/img/gallery/cool_architecture.jpg',
    description: 'These architects are killing it!!!',
    likes: ['test', 'test2'],
    comments: [{ user: 'test2', body: 'wow 😲' }],
  },
];

const photosContainer = document.querySelector('.photos');

function generateComments(comments) {
  return comments
    .map(
      (comment) =>
        `<div class="comment"><b>${comment.user.slice(0, 5)}</b> ${
          comment.body
        }</div>`
    )
    .join('\n');
}

function createPhotoHtml(id, imgUrl, imgDescription, likes, isLiked, comments) {
  return `<div class="photo-card flex flex-column" data-id="${id}">
    <div class="photo-container">
      <img
      src="${imgUrl}"
      alt="${imgDescription}"
      />
    </div>
    <div class="action-buttons">
      <span class="material-icons like-btn ${
        isLiked ? 'liked' : ''
      }"> favorite </span>
      <span class="material-icons"> chat </span>
    </div>
    <div class="likes">${likes} likes</div>
    <span class="description">${imgDescription}</span>
    <div class="comments m-y-1">
      ${generateComments(comments)}
    </div>
    <div class="new-comment">
      <input type="text" placeholder="Add a comment..." />
      <span class="send-comment material-icons"> send </span>
    </div>
  </div>`;
}

function updatePhotoLikes(photoId, user, isNew) {
  const photos = JSON.parse(localStorage.getItem('photos'));
  const likes = photos[photoId].likes;

  if (isNew) {
    likes.push(user);
  } else {
    likes.splice(likes.indexOf(user), 1);
  }

  localStorage.setItem('photos', JSON.stringify(photos));

  updateGallery();
}

function updatePhotoComments(photoId, comment) {
  const photos = JSON.parse(localStorage.getItem('photos'));
  photos[photoId].comments.push(comment);

  localStorage.setItem('photos', JSON.stringify(photos));

  updateGallery();
}

function addListeners() {
  document.querySelectorAll('.like-btn').forEach((likeBtn) =>
    likeBtn.addEventListener('click', (event) => {
      const photoId =
        event.target.parentNode.parentNode.getAttribute('data-id');
      const userId = sessionStorage.getItem('user_id');

      updatePhotoLikes(
        photoId,
        userId,
        !event.target.classList.contains('liked')
      );
    })
  );

  document.querySelectorAll('.new-comment .send-comment').forEach((input) =>
    input.addEventListener('click', (event) => {
      const commentRef = event.target.parentNode.querySelector('input');

      if (!commentRef.value) {
        return;
      }

      const photoId =
        event.target.parentNode.parentNode.getAttribute('data-id');

      updatePhotoComments(photoId, {
        user: sessionStorage.getItem('user_id'),
        body: commentRef.value,
      });

      commentRef.value = '';
    })
  );
}

function updateGallery() {
  const userId = sessionStorage.getItem('user_id');
  const photos = JSON.parse(localStorage.getItem('photos'));

  let photosHtml = '';

  for (const photo of photos) {
    const isPhotoLiked = photo.likes.find((users) => users === userId);
    photosHtml += createPhotoHtml(
      photo.id,
      photo.url,
      photo.description,
      photo.likes.length,
      isPhotoLiked,
      photo.comments
    );
  }

  photosContainer.innerHTML = photosHtml;

  addListeners();
}

function init() {
  const userId = sessionStorage.getItem('user_id');

  if (!userId) {
    sessionStorage.setItem('user_id', crypto.randomUUID());
  }

  const photos = localStorage.getItem('photos');

  if (!photos) {
    localStorage.setItem('photos', JSON.stringify(photosData));
  }

  updateGallery();
}

init();
