const menuAnchor = document.querySelector('.menuicon');
const linksList = document.querySelector('.links-list');

document.querySelector('.menuicon').addEventListener('click', () => {
  const menuState =
    menuAnchor.getAttribute('data-active') === 'true' ? true : false;

  menuAnchor.setAttribute('data-active', !menuState);
  linksList.setAttribute('data-open', !menuState);
});
