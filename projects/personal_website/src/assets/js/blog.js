const newBlogRef = document.querySelector('form');
const postsContainer = document.querySelector('.posts-container');

function updatePosts() {
  const posts = JSON.parse(localStorage.getItem('posts'));

  const mappedPosts = posts.map((post) => {
    const date = new Date(post.date).toLocaleString().split(',');
    return `<div class="blog-post"><div class="m-b-1"><h2>${
      post.title
    }</h2><span class="post-date">${date[0]} ${date[1].slice(
      0,
      5
    )} ${date[1].slice(-2)}</span></div><p>${post.body.slice(
      0,
      100
    )}...</p><a href="#">Read More...</a></div>`;
  });

  postsContainer.innerHTML = mappedPosts.join('\n');
}

function init() {
  if (!localStorage.getItem('posts')) {
    localStorage.setItem('posts', JSON.stringify([]));
  }

  updatePosts();
}

function createNewPost(title, body) {
  const prevPosts = JSON.parse(localStorage.getItem('posts'));

  prevPosts.unshift({ id: prevPosts.length, title, body, date: new Date() });

  localStorage.setItem('posts', JSON.stringify(prevPosts));
}

newBlogRef.addEventListener('submit', (event) => {
  event.preventDefault();

  const postTitle = document.querySelector('input[name="blog-title"]');
  const postBody = document.querySelector('textarea[name="blog-content"]');

  if (postTitle.value === '' || postBody.value === '') {
    alert("You can't submit a post with empty inputs");
    return;
  }

  createNewPost(postTitle.value, postBody.value);

  postTitle.value = '';
  postBody.value = '';

  updatePosts();
});

init();
