# The race between the turtle and the rabbit

## Rules
Whoever crosses 70 (or more) places first, wins the game. The game ends only and only when there's a single winner. If the turtle and the rabbit meet at the same position the game displays **OUCH**, and continues.

## Terminology
`n` places to the **left** means backwards od to the **left** edge of the web page -> losing progress

`n` places to the **right** means backwards od to the **left** edge of the web page -> gaining progress

## Allowed Moves
### Turtle
- `fast plod` -> 3 places to the **right** (50% chance)
- `slip` -> 6 places to the **left** (20% chance)
- `slow plod` -> 1 place to the **right** (30% chance)

### Rabbit
- `sleep` -> 0 places, keeps the same position (20% chance)
- `big hop` -> 9 places to the **right** (20% chance)
- `big slip` -> 12 places to the **left** (10% chance)
- `small hop` ->  1 place to the **right** (30% chance)
- `small slip` -> 2 places to the **left** (20% chance)