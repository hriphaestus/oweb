const startBtn = document.querySelector('.start-btn');
const resetBtn = document.querySelector('.reset-btn');
const ouch = document.querySelector('.ouch');
const winnerPanel = document.querySelector('.winner');

const turtlePositions = document.querySelectorAll('.turtle-pos');
const rabbitPositions = document.querySelectorAll('.rabbit-pos');
const turtleStats = document.querySelector('.turtle-stats');
const rabbitStats = document.querySelector('.rabbit-stats');

function getWinner(turtlePos, rabbitPos) {
  if (turtlePos >= 70) {
    return 'turtle';
  }

  if (rabbitPos >= 70) {
    return 'rabbit';
  }

  return null;
}

function getTurtleNextMove(chance) {
  if (chance < 0.5) {
    return 3;
  }

  if (chance < 0.7) {
    return -6;
  }

  return 1;
}

function getRabbitNextMove(chance) {
  if (chance < 0.2) {
    return 0;
  }

  if (0.2 <= chance && chance < 0.4) {
    return 9;
  }

  if (0.4 <= chance && chance < 0.5) {
    return -12;
  }

  if (0.5 <= chance && chance < 0.8) {
    return 1;
  }

  return -2;
}

function updateStats(turtlePos, rabbitPos) {
  turtleStats.innerText = turtlePos.current;
  rabbitStats.innerText = rabbitPos.current;
}

function drawPositions(turtlePos, rabbitPos) {
  if (turtlePos.current < 1) {
    if (turtlePos.previous > 0)
      turtlePositions[turtlePos.previous - 1].style.backgroundColor = 'white';
  } else {
    if (turtlePos.previous > 0)
      turtlePositions[turtlePos.previous - 1].style.backgroundColor = 'white';
    if (turtlePos.current < 70)
      turtlePositions[turtlePos.current - 1].style.backgroundColor =
        'var(--turtle-color)';
  }

  if (rabbitPos.current < 1) {
    if (rabbitPos.previous > 0)
      rabbitPositions[rabbitPos.previous - 1].style.backgroundColor = 'white';
  } else {
    if (rabbitPos.previous > 0)
      rabbitPositions[rabbitPos.previous - 1].style.backgroundColor = 'white';
    if (rabbitPos.current < 70)
      rabbitPositions[rabbitPos.current - 1].style.backgroundColor =
        'var(--rabbit-color)';
  }
}

function main() {
  startBtn.classList.add('hidden');
  resetBtn.classList.remove('hidden');

  let turtlePos = { current: 1, previous: 1 };
  let rabbitPos = { current: 1, previous: 1 };

  const intervalHandle = setInterval(() => {
    const chance = Math.random();

    turtlePos.current += getTurtleNextMove(chance);
    rabbitPos.current += getRabbitNextMove(chance);

    updateStats(turtlePos, rabbitPos);
    drawPositions(turtlePos, rabbitPos);

    turtlePos.previous = turtlePos.current;
    rabbitPos.previous = rabbitPos.current;

    const winner = getWinner(turtlePos.current, rabbitPos.current);

    if (winner) {
      clearInterval(intervalHandle);
      winnerPanel.parentElement.classList.remove('hidden');
      winnerPanel.innerText = 'the ' + winner + ' won';
      winnerPanel.classList.add('reveal');
      setTimeout(() => {
        winnerPanel.classList.remove('reveal');
        winnerPanel.addEventListener('transitionend', () =>
          winnerPanel.parentElement.classList.add('hidden')
        );
      }, 5000);
    }

    if (turtlePos.current === rabbitPos.current) {
      ouch.classList.add('reveal');
      setTimeout(() => ouch.classList.remove('reveal'), 2000);
    }
  }, 1000);
}

startBtn.addEventListener('click', main, false);
resetBtn.addEventListener('click', () => location.reload(), false);
