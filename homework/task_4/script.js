let activeFilter;

function filterCategory(category) {
  if (activeFilter != category) {
    // reset results list
    $(".filter-cat-results .f-cat").removeClass("active");
    $(".inverted-filter-cat-results .f-cat").addClass("active");

    // elements to be filtered
    $(".filter-cat-results .f-cat")
      .filter('[data-cat="' + category + '"]')
      .addClass("active");

    $(".inverted-filter-cat-results .f-cat")
      .filter('[data-cat="' + category + '"]')
      .removeClass("active");

    // reset active filter
    activeFilter = category;
    $(".filtering button").removeClass("active");
  }
}

$(".f-cat").addClass("active");

$(".filtering button").click(function () {
  if ($(this).hasClass("cat-all")) {
    $(".filter-cat-results .f-cat").addClass("active");
    $(".inverted-filter-cat-results .f-cat").addClass("active");
    activeFilter = "cat-all";
    $(".filtering button").removeClass("active");
  } else {
    filterCategory($(this).attr("data-cat"));
  }
  $(this).addClass("active");
});
